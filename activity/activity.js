db.users.insertMany([
  {
    firstName: "Pegasus",
    lastName: "Jully",
    email: "jullyp@gmail.com",
    password: "1010101",
    isAdmin: false,
  },
  {
    firstName: "Jupiter",
    lastName: "Raymond",
    email: "kam20@gmail.com",
    password: "02985",
    isAdmin: false,
  },
  {
    firstName: "Dorian",
    lastName: "Punchy",
    email: "dorianP",
    password: "053342",
    isAdmin: false,
  },
  {
    firstName: "Shu",
    lastName: "Sementic",
    email: "shushu@gmail.com",
    password: "098532",
    isAdmin: false,
  },
  {
    firstName: "Tornady",
    lastName: "Diamond",
    email: "tdd@gmail.com",
    password: "98523",
    isAdmin: false,
  },
]);

db.newCourse.insertMany([
  {
    name: "Intermediate",
    price: 1000,
    isActive: false,
  },
  {
    name: "Middle",
    price: 3000,
    isActive: false,
  },
  {
    name: "God",
    price: 310000,
    isActive: false,
  },
]);

db.users.find({ isAdmin: false });

db.users.updateOne({}, { $set: { isAdmin: true } });
db.newCourse.update({}, { $set: { isActive: true } });

db.newCourse.deleteMany({isActive:false});
